package org.geektimes.configuration.microprofile.config.source.servlet;

import org.geektimes.configuration.microprofile.config.source.MapBasedConfigSource;

import javax.servlet.ServletContext;
import java.util.Enumeration;
import java.util.Map;

public class ServletContextConfigSource extends MapBasedConfigSource {

    public ServletContextConfigSource() {
        super("ServletContext Init Parameters", 500);
    }

    @Override
    protected void prepareConfigData(Map configData) throws Throwable {
        ServletContext servletContext = ThreadLocalServletContext.servletContextThreadLocal.get();
        Enumeration<String> parameterNames = servletContext.getInitParameterNames();
        while (parameterNames.hasMoreElements()) {
            String parameterName = parameterNames.nextElement();
            configData.put(parameterName, servletContext.getInitParameter(parameterName));
        }
        ThreadLocalServletContext.servletContextThreadLocal.remove();
    }
}
