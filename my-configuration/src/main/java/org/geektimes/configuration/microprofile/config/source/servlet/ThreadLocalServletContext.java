package org.geektimes.configuration.microprofile.config.source.servlet;

import javax.servlet.ServletContext;

/**
 * @Author 曾虎
 * @Date 2021/3/24
 */
public class ThreadLocalServletContext {
    /**
     * 用户扩展ServletContext配置源传递servletcontext
     */
    static final ThreadLocal<ServletContext> servletContextThreadLocal = new ThreadLocal<ServletContext>();
}
