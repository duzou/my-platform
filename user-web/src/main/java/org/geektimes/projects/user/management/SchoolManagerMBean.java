package org.geektimes.projects.user.management;

import org.geektimes.projects.user.domain.User;

/**
 * {@link User} MBean 接口描述
 */
public interface SchoolManagerMBean {

    Integer getMaxClassNumber();

    void setMaxClassNumber(Integer maxClassNumber);

    String getSchoolName();

    void setSchoolName(String schoolName);

    Integer getMaxClassPersionNumber();

    void setMaxClassPersionNumber(Integer maxClassPersionNumber);

    Integer getMinClassPersionNumber();

    void setMinClassPersionNumber(Integer minClassPersionNumber);

    String toString();
}
