package org.geektimes.projects.user.validator.bean.validation.constraintvalidation;

import org.geektimes.projects.user.validator.bean.validation.constraint.LengthValidator;
import org.geektimes.projects.user.validator.bean.validation.constraint.PhoneValidator;
import org.geektimes.projects.user.validator.bean.validation.util.ValidatorUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author 曾虎
 * @Date 2021/3/10
 */
public class LengthValidatorInner implements ConstraintValidator<LengthValidator, String> {
    int minLength;
    int maxLength;

    @Override
    public void initialize(LengthValidator constraintAnnotation) {
        minLength=constraintAnnotation.minLength();
        maxLength=constraintAnnotation.maxLength();
    }

    /**
     * 校验逻辑的实现
     *
     * @param value 需要校验的 值
     * @return 布尔值结果
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            if(null!=value){
                if(value.length()>=minLength && value.length()<=32){
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
