package org.geektimes.projects.user.management;

import org.geektimes.web.mvc.context.ComponentContext;

import javax.annotation.PostConstruct;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * 用于启动的时候注册mbean
 */
public class RegisterMBean {
    private ComponentContext componentContext;

    @PostConstruct
    public void init(){
        componentContext=ComponentContext.getInstance();
        School school = new School();
        // 为 UserMXBean 定义 ObjectName
        ObjectName objectName = null;
        try {
            objectName = new ObjectName("org.geektimes.projects.user.management:type=School");
            componentContext.getmBeanServer().registerMBean(createSchoolMBean(school), objectName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SchoolManager createSchoolMBean(School school){
        return new SchoolManager(school);
    }

}
