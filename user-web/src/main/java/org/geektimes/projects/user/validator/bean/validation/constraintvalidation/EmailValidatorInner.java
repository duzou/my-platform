package org.geektimes.projects.user.validator.bean.validation.constraintvalidation;

import org.geektimes.projects.user.validator.bean.validation.constraint.EmailValidator;
import org.geektimes.projects.user.validator.bean.validation.util.ValidatorUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author 曾虎
 * @Date 2021/3/10
 */
public class EmailValidatorInner implements ConstraintValidator<EmailValidator, String> {


    @Override
    public void initialize(EmailValidator constraintAnnotation) {
    }

    /**
     * 校验逻辑的实现
     *
     * @param value 需要校验的 值
     * @return 布尔值结果
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            return ValidatorUtil.isEmail(value);
        } catch (Exception e) {
            return false;
        }
    }
}
