package org.geektimes.projects.user.validator.bean.validation.constraint;

import org.geektimes.projects.user.validator.bean.validation.constraintvalidation.EmailValidatorInner;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author 曾虎
 * @Date 2021/3/10
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {EmailValidatorInner.class})
public @interface EmailValidator {

    // 约束注解验证时的输出消息
    String message() default "邮箱不合法";

    /**
     * 必须的属性
     * 用于分组校验
     */
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
