package org.geektimes.projects.user.management;

public class School {
    private Integer maxClassNumber;
    private String schoolName;
    private Integer maxClassPersionNumber;
    private Integer minClassPersionNumber;

    public Integer getMaxClassNumber() {
        return maxClassNumber;
    }

    public void setMaxClassNumber(Integer maxClassNumber) {
        this.maxClassNumber = maxClassNumber;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Integer getMaxClassPersionNumber() {
        return maxClassPersionNumber;
    }

    public void setMaxClassPersionNumber(Integer maxClassPersionNumber) {
        this.maxClassPersionNumber = maxClassPersionNumber;
    }

    public Integer getMinClassPersionNumber() {
        return minClassPersionNumber;
    }

    public void setMinClassPersionNumber(Integer minClassPersionNumber) {
        this.minClassPersionNumber = minClassPersionNumber;
    }

    @Override
    public String toString() {
        return "School{" +
                "maxClassNumber=" + maxClassNumber +
                ", schoolName='" + schoolName + '\'' +
                ", maxClassPersionNumber=" + maxClassPersionNumber +
                ", minClassPersionNumber=" + minClassPersionNumber +
                '}';
    }
}
