package org.geektimes.projects.user.management;

public class SchoolManager implements SchoolManagerMBean {

    private final School school;

    public SchoolManager(School school) {
        this.school = school;
    }

    @Override
    public Integer getMaxClassNumber() {
        return school.getMaxClassNumber();
    }

    @Override
    public void setMaxClassNumber(Integer maxClassNumber) {
        school.setMaxClassNumber(maxClassNumber);
    }

    @Override
    public String getSchoolName() {
        return school.getSchoolName();
    }

    @Override
    public void setSchoolName(String schoolName) {
        school.setSchoolName(schoolName);
    }

    @Override
    public Integer getMaxClassPersionNumber() {
        return school.getMaxClassPersionNumber();
    }

    @Override
    public void setMaxClassPersionNumber(Integer maxClassPersionNumber) {
        school.setMaxClassPersionNumber(maxClassPersionNumber);
    }

    @Override
    public Integer getMinClassPersionNumber() {
        return school.getMinClassPersionNumber();
    }

    @Override
    public void setMinClassPersionNumber(Integer minClassPersionNumber) {
        school.setMinClassPersionNumber(minClassPersionNumber);
    }

    public School getSchool() {
        return school;
    }
}
