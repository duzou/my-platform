package org.geektimes.web.mvc.annotation;


import java.lang.annotation.*;

/**
 * 标记controller
 * @author 曾虎
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Controller {
}
