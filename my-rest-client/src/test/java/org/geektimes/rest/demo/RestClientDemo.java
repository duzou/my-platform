package org.geektimes.rest.demo;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Response;
import java.net.URI;

public class RestClientDemo {

    public static void main(String[] args) {
        get();
        post();
    }

    public static void get(){
        System.out.println("get begin-----");
        String url="http://127.0.0.1:8080/hello/word";
        Client client = ClientBuilder.newClient();
        WebTarget webTarget=client.target(url);
        Invocation.Builder builder=webTarget.request();
        Response response = builder.get();

        String content = response.readEntity(String.class);
        System.out.println(content);
        System.out.println("get end-----");
    }

    public static void post(){
        System.out.println("post begin-----");
        String url="http://127.0.0.1:8080/register";
        Client client = ClientBuilder.newClient();
        WebTarget webTarget=client.target(url);
        Invocation.Builder builder=webTarget.request();
        Response response = builder.post(Entity.json("{\"username\": \"123\",\"password\": \"ddddaaaddd\"}"));
        String content = response.readEntity(String.class);
        System.out.println(content);
        System.out.println("post end-----");
    }
}
