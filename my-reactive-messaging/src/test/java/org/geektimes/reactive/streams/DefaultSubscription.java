package org.geektimes.reactive.streams;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

/**
 * Subscription 与 Subscriber 是一一对应
 */
public class DefaultSubscription implements Subscription {

    private boolean canceled = false;

    /**
     * 设置最大的请求处理数（默认3
     */
    public long maxRequestHandle=3;

    /**
     * 已经处理的请求数
     */
    public long requestHandleCount=0;

    @Override
    public void request(long n) {
        this.maxRequestHandle=n;
    }

    @Override
    public void cancel() {
        canceled = true;
    }

    public boolean isCanceled() {
        return canceled;
    }
}
