### 第六周作业
#### 通过 Lettuce 实现一套 Redis CacheManager 以及 Cache
> org.geektimes.cache.CachingTest#testSampleLettuce可以进行测试

### 第五周作业
#### 修复my-reactive-messaging模块
> 改模块修复已解决，并且实现了可以通过Subscription.request()设置当前订阅者最大接受的消息数
> 
#### 继续完善 my-rest-client POST 方法
> 在my-rest-client模块的invocation中新增HttpPostinvocation类用于处理post请求，目前只针对json格式的body调试没有问题，其他格式暂时没有进行调试。
>
> 测试类在test目录下RestClientDemo.post方法


### 第四周作业
##### 完善 my-configuration 模块
> 在 ServletContext 获取如何通过 ThreadLocal 获取
> 
> 由于Listener获取classload有问题所有把初始化config的逻辑放在了ServletContainerInitializer中

### 第三周作业
##### 需求一：整合 jolokia
> 只需要在maven中引入jolokia的包，并且在web.xml中配置jolokia的servlet拦截就可以通过http查看jmx的信息
>
> 自定义的JMX MBean在org.geektimes.projects.user.management目录下，并且在项目启动的时候通过jndi进行mbean的注册
>
##### 需求二：关于继续完成 Microprofile config API
> 代码在org.geektimes.configuration.microprofile.config包下面
>
> 首先在程序启动中的ComponentContext通过SPI加载ConfigProviderResolver，
> 在ConfigProviderResolver中通过jndi加载Config
> 在Config通过SPI加载配置和类型转换器
> 
> 目前的配置支持获取通过-D设置的启动参数以及获取classes中的project.properties配置文件中的值
>
> 配置的加载顺序使用ConfigSource中的getOrdinal设置优先级
> 
> 类型装换目前只支持Integer,String,Double


### 第二周作业
> 所有的自定义校验器都在`org.geektimes.projects.user.validator.bean.validation.constraint`这个目录下面
> 目前实现了邮箱，手机号，长度3个自定义校验
>

### 第一周作业

> 在项目web.xml文件配置controller的扫描路径，提取路径下带有@Controller的所有类作为controller，并且支持一个Controller可以进行多个`路径-方法`映射
>
> 为了实现controller的依赖注入就把上下文放到了web-mvc里面，由servelt的init来负责初始化上下文，以及bean装载和注入
>


